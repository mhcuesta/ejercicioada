--Programa en ADA
with Ada.Text_IO;
use Ada.Text_IO;

procedure hola_de_Maria is
  procedure hola_mario is
  begin
     Ada.Text_IO.Put_Line("Hola mundo de Mario");
end hola_mario;

begin
   -- aquí salida de texto
   hola_mario;
   Ada.Text_IO.Put_Line("Este es el Hola mundo de Maria");
end hola_de_Maria;



